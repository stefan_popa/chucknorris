package com.example.chucknorris.di;

import android.app.Application;
import android.content.Context;

import com.example.chucknorris.R;
import com.example.chucknorris.service.ChuckNorrisService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

@Module
public abstract class AppModule {

    @Binds
    abstract Context provideContext(Application application);

    @Provides
    static ChuckNorrisService provideChuckNorrisService(Retrofit retrofit) {
        return retrofit.create(ChuckNorrisService.class);
    }

    @Provides
    static Gson provideGson() {
        return new GsonBuilder()
                .serializeNulls()
                .create();
    }

    @Provides
    static OkHttpClient provideOkHttpClient() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Timber.tag("HttpLogging").d(message);
            }
        });

        return new OkHttpClient.Builder()
                .addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BASIC))
                .build();
    }

    @Provides
    static Retrofit provideRetrofit(Context context, OkHttpClient client) {
        String baseUrl = context.getString(R.string.chuck_norris_base_url);
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
