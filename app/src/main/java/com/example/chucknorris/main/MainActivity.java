package com.example.chucknorris.main;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.chucknorris.R;
import com.google.android.material.button.MaterialButton;

import javax.inject.Inject;

public class MainActivity extends DaggerAppCompatActivity implements MainView {

    @Inject
    MainPresenter presenter;

    @BindView(R.id.jokeTextView)
    TextView jokeTextView;

    @BindView(R.id.newJokeButton)
    MaterialButton newJokeButton;

    @BindView(R.id.imageView)
    ImageView imageView;

    @OnClick(R.id.newJokeButton)
    void onNewJokeButtonClicked() {
        this.newJokeButton.setEnabled(false);
        this.presenter.onUpdateJoke();
    }

    @OnClick(R.id.jokeTextView)
    void onjokeTextViewClicked() {
        presenter.onNavigateToJokePage();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        presenter.onViewIsReady();
    }

    @Override
    public void updateWith(MainViewModel viewModel) {
        newJokeButton.setEnabled(true);

        jokeTextView.setText(viewModel.getJoke());

        Glide.with(this)
                .load(viewModel.getIconUrl())
                .centerInside()
                .into(imageView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDestroyed();
    }

    @Override
    public void navigateTo(Uri uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
