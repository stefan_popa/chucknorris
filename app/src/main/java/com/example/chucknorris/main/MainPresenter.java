package com.example.chucknorris.main;

interface MainPresenter {

    void onViewIsReady();

    void onViewDestroyed();

    void onUpdateJoke();

    void onNavigateToJokePage();
}
