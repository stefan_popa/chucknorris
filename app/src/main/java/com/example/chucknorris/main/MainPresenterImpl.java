package com.example.chucknorris.main;

import android.net.Uri;

import com.example.chucknorris.service.ChuckNorrisService;
import com.example.chucknorris.service.RandomJokeResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainPresenterImpl implements MainPresenter {

    private CompositeDisposable compositeSubscriptions = new CompositeDisposable();

    private ChuckNorrisService service;
    private MainView view;

    private MainViewModel viewModel;

    MainPresenterImpl(MainView view, ChuckNorrisService service) {
        this.service = service;
        this.view = view;
    }

    @Override
    public void onViewIsReady() {
        onUpdateJoke();
    }

    @Override
    public void onUpdateJoke() {
        compositeSubscriptions.add(service.fetchRandomJoke()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onResponseRetrieved, Throwable::printStackTrace));
    }

    @Override
    public void onNavigateToJokePage() {
        if (viewModel != null) {
            view.navigateTo(Uri.parse(viewModel.getJokeUrl()));
        }
    }

    @Override
    public void onViewDestroyed() {
        compositeSubscriptions.clear();
    }

    private void onResponseRetrieved(RandomJokeResponse response) {
        String iconUrl = "https://assets.chucknorris.host/img/chucknorris_logo_coloured_small@2x.png";
        viewModel = new MainViewModel(response.value, iconUrl, response.url);
        view.updateWith(viewModel);
    }
}
