package com.example.chucknorris.main;

import android.net.Uri;

interface MainView {
    void updateWith(MainViewModel viewModel);

    void navigateTo(Uri uri);
}
