package com.example.chucknorris.main;

public class MainViewModel {

    private String joke;
    private String iconUrl;
    private String jokeUrl;

    public MainViewModel(String joke, String iconUrl, String jokeUrl) {
        this.joke = joke;
        this.iconUrl = iconUrl;
        this.jokeUrl = jokeUrl;
    }

    public String getJoke() {
        return joke;
    }

    public void setJoke(String joke) {
        this.joke = joke;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getJokeUrl() {
        return jokeUrl;
    }

    public void setJokeUrl(String jokeUrl) {
        this.jokeUrl = jokeUrl;
    }
}
