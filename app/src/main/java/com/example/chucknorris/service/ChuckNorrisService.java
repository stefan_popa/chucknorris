package com.example.chucknorris.service;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface ChuckNorrisService {

    @GET("jokes/random")
    Single<RandomJokeResponse> fetchRandomJoke();

}
