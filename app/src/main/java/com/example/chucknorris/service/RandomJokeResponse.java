package com.example.chucknorris.service;

import com.google.gson.annotations.SerializedName;

public class RandomJokeResponse {

    @SerializedName("icon_url")
    public String iconUrl;
    public String id;
    public String url;
    public String value;

    public RandomJokeResponse(String iconUrl, String id, String url, String value) {
        this.iconUrl = iconUrl;
        this.id = id;
        this.url = url;
        this.value = value;
    }
}
