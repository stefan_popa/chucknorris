package com.example.chucknorris.main;

import com.example.chucknorris.service.ChuckNorrisService;
import com.example.chucknorris.service.RandomJokeResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.android.plugins.RxAndroidPlugins;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainPresenterImplTest {

    @Mock
    MainView view;

    @Mock
    ChuckNorrisService service;

    private MainPresenterImpl systemUnderTest;

    @Before
    public void setUp() throws Exception {
        RxJavaPlugins.reset();
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> Schedulers.trampoline());

        systemUnderTest = new MainPresenterImpl(view, service);
    }

    @After
    public void tearDown() throws Exception {
        systemUnderTest = null;
    }

    @Test
    public void test_when_view_is_ready_new_joke_is_fetched() {
        RandomJokeResponse response = new RandomJokeResponse("test",
                "test", "test", "test");

        when(service.fetchRandomJoke()).thenReturn(Single.just(response));

        systemUnderTest.onViewIsReady();

        verify(view).updateWith(any());
    }

}